from flask import Flask, render_template
from flask_socketio import SocketIO, emit

app = Flask(__name__)
socketio = SocketIO(app)

@app.route("/")
def index():
    return render_template("index.html")

@socketio.on("connect")
def connection():
    print("--------------------------------")

if __name__ == "__main__":
    app.debug = True
    socketio.run(app,host="0.0.0.0",port=8000)