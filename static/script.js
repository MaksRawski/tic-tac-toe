const socket = io();
let c=document.getElementById('can').getContext("2d");
document.getElementById('can').addEventListener('click',(e)=>{
    draw(turn,getArea(e));
});

let score={"x":0,"o":0};
let board=[[],[],[]];

getArea=(evnt)=>{
    return{
        x:Math.floor(evnt.offsetX/100),
        y:Math.floor(evnt.offsetY/100)
    };
}
//draw board
drawBoard=()=>{
    turn="x"
    board=[[],[],[]];
    c.fillStyle="#000";
    c.fillRect(100,0,5,300);
    c.fillRect(200,0,5,300);
    c.fillRect(0,100,300,5);
    c.fillRect(0,200,300,5);
}

draw=(figure,pos)=>{
    c.font="150px sans";
    if(board[pos.y][pos.x]===undefined){
        if(figure=="x") board[pos.y][pos.x]=1;
        else board[pos.y][pos.x]=-1;
        if(turn=="x"){turn="o"; c.fillStyle="#fff";}
        else{turn="x"; c.fillStyle="#16a085"}
        c.fillText(figure,pos.x*100+7,(pos.y+1)*100-5)
        checkWin();
        if(board[0].length+board[1].length+board[2].length==9){
            setTimeout(()=>{
                c.clearRect(0,0,300,300);
                drawBoard();
            },3000);
            
        }
    }
}

checkWin=()=>{
    for(let i=0;i<3;i++){
        //rows
        if(board[i].reduce((a, b) => a + b, 0)==3) win("x")
        else if(board[i].reduce((a, b) => a + b, 0)==-3) win("o");
        //columns
        if(board[0][i]+board[1][i]+board[2][i]==3) win("x");
        else if(board[0][i]+board[1][i]+board[2][i]==-3) win("o");
    }
    //diagonals
    if(board[0][0]+board[1][1]+board[2][2]==3) win("x")
    else if(board[0][0]+board[1][1]+board[2][2]==-3) win("o")
    if(board[0][2]+board[1][1]+board[2][0]==3) win("x")
    else if(board[0][2]+board[1][1]+board[2][0]==-3) win("o")
}

win=(p)=>{
        score[p]++;
        console.log(p);
        document.getElementById(p+"-score").innerHTML++;
        c.clearRect(0, 0, 300,300);
        drawBoard();
}
drawBoard();